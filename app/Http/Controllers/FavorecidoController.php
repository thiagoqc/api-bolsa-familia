<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Saque;
use App\Models\Favorecido;
use Validator;
use Response;
use DB;

class FavorecidoController extends Controller
{

    public function new(Request $request)
    {
        $nis_favorecido = (int) $request->nis_favorecido;

        $favorecido = Favorecido::find($nis_favorecido);

        if($favorecido != null){
            return Response::json([
                'success' => False,
                'message' => 'Um favorecido com esse nis ja existe'
            ]);
        } else {

            $favorecido = new Favorecido;

            $favorecido->nis_favorecido = (int) $request->nis_favorecido;
            $favorecido->nome = $request->nome;

            $favorecido->save();

            return Response::json([
                'success'        => True,
                'message'        => 'Favorecido criado com sucesso',
                'nis_favorecido' => $favorecido->nis_favorecido,
                'nome'           => $favorecido->nome
            ]);
        }


    }

    public function update(\Illuminate\Http\Request $request)
    {
        $nis_favorecido = $request->nis_favorecido;

        $favorecido = Favorecido::find($nis_favorecido);

        $favorecido->nome = $request->nome;

        try {
            $favorecido->save();

            return Response::json([
                'success' => True,
                'message' => 'Favorecido atualizado com sucesso'
            ]);
        } catch(\Exception $e) {

            return Response::json([
                'success' => False,
                'message' => 'Nao foi possivel atualizar o favorecido'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $nis_favorecido = $request->nis_favorecido;

        $favorecido = Favorecido::find($nis_favorecido);

        try {
            $favorecido->delete();

            return Response::json([
                'success' => True,
                'message' => 'Favorecido excluído com sucesso'
            ]);
        } catch(\Exception $e) {

            return Response::json([
                'success' => False,
                'message' => 'Nao foi possivel excluir o favorecido'
            ]);
        }

    }
}
