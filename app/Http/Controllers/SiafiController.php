<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Saque;
use App\Models\Favorecido;
use App\Models\Siafi;
use Response;
use DB;

class SiafiController extends Controller
{

    public function topState()
    {
        $siafis = Siafi::all();
        $sum    = array();

        foreach($siafis as $siafi) {
            foreach($siafi->saques as $saque) {
                if(array_key_exists($siafi->uf, $sum))
                    $sum[$siafi->uf] += $saque->valor_parcela;
                else
                    $sum[$siafi->uf] = $saque->valor_parcela;
            }
        }

        $value = max($sum);

        $key = array_search($value, $sum);

        return Response::json([
            'uf'    => $key,
            'valor' => $value
        ]);

    }

    public function eachState()
    {
        $siafis = Siafi::all();
        $sum    = array();

        foreach($siafis as $siafi) {
            foreach($siafi->saques as $saque) {
                if(array_key_exists($siafi->uf, $sum))
                    $sum[$siafi->uf] += $saque->valor_parcela;
                else
                    $sum[$siafi->uf] = $saque->valor_parcela;
            }
        }

        return Response::json($sum);
    }

    public function topMunicipio()
    {
        $siafis = Siafi::all();
        $sum    = array();

        foreach($siafis as $siafi) {
            foreach($siafi->saques as $saque) {
                if(array_key_exists($siafi->nome_municipio, $sum))
                    $sum[$siafi->nome_municipio] += $saque->valor_parcela;
                else
                    $sum[$siafi->nome_municipio] = $saque->valor_parcela;
            }
        }

        $value = max($sum);

        $key = array_search($value, $sum);

        return Response::json([
            'municipio' => $key,
            'valor'     => $value,
            'estado'    => 'SP'
        ]);

    }

}
