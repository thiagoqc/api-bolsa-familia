<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Saque;
use App\Models\Favorecido;
use Response;
use DB;

class SaqueController extends Controller
{
    public function all()
    {
        $results = DB::select('SELECT * FROM tb_favorecido_saque LIMIT 10');
        // $results = Saque::all();

        return Response::json($results);
    }

    public function topFavored()
    {
        $max = DB::select('SELECT t.codigo_siafi, t.nis_favorecido, t.valor_parcela, data
                            FROM ( SELECT MAX(valor_parcela) AS max_parcela FROM tb_favorecido_saque ) AS m
                                INNER
                                    JOIN tb_favorecido_saque AS t
                                    ON t.valor_parcela = m.max_parcela');

        $favored = Favorecido::find($max[0]->nis_favorecido);

        return Response::json([
            'nis_favorecido' => $favored->nis_favorecido,
            'nome'           => $favored->nome,
            'valor_parcela'  => $max[0]->valor_parcela,
            'codigo_siafi'   => $max[0]->codigo_siafi,
            'data'           => $max[0]->data
        ]);

    }
}
