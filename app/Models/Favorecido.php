<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorecido extends Model
{
    protected $fillable = [
        'nis_favorecido',
        'nome'
    ];

    protected $table = 'tb_favorecido';

    protected $primaryKey = 'nis_favorecido';

    public $timestamps = false;

    // Relationships

    public function saques()
    {
        return $this->hasMany(Saque::class, 'nis_favorecido');
    }
}
