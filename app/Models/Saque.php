<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Saque extends Model
{
    protected $fillable = [
        'nis_favorecido',
        'codigo_siafe',
        'valor_parcela',
        'data'
    ];

    protected $table = 'tb_favorecido_saque';

    public $timestamps = false;


    // Relationships

    public function favorecido()
    {
        return $this->belongsTo(Favorecido::class);
    }

    public function siafi()
    {
        return $this->belongsTo(Siafi::class);
    }
}
