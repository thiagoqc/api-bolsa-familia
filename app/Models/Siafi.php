<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Siafi extends Model
{
    protected $fillable = [
        'uf',
        'codigo_siafi',
        'nome_municipio',
        'nis_favorecido'
    ];

    protected $table = 'tb_siafi';

    protected $primaryKey = 'codigo_siafi';

    public $timestamps = false;

    // Relationships

    public function saques()
    {
        return $this->hasMany(Saque::class, 'codigo_siafi');
    }
}
