<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/maior-favorecido', 'SaqueController@topFavored');

Route::get('/maior-estado-favorecido', 'SiafiController@topState');

Route::get('/gasto-por-estado', 'SiafiController@eachState');

Route::get('/maior-municipio-favorecido', 'SiafiController@topMunicipio');

Route::post('/inserir-favorecido', 'FavorecidoController@new');

Route::post('/atualizar-favorecido', 'FavorecidoController@update');

Route::post('/deletar-favorecido', 'FavorecidoController@delete');

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
